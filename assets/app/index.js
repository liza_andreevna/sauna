window.onload = function () {
    let price = {
        totalAmount: 0,
        peopleAmount: 0,
        peoplesCount: 0,
        hoursCount: 0
    };

    //Шаблон для рендеринга
    function WritePrice(priceData) {
        return `
            <p> Общая сумма: ${priceData.totalAmount}</p>
            <p> С каждого человека: ${priceData.peopleAmount.toFixed(2)}</p>
            `
    }

    //Вывод результата
    function CountAmount(priceData) {
        priceData.peoplesCount = parseInt(document.getElementById("peoples").value);
        priceData.hoursCount = parseInt(document.getElementById("hours").value);
        CalcTotalAmount(arr, priceData);
        update();
    }

    //Подсчёт общей стоимости
    function CalcTotalAmount(arr, priceData) {
        let elem = arr.find(i => i.days == "Fri-Sun");
        let hours = priceData.hoursCount;
        let peoples = priceData.peoplesCount;
        HoursAmount(elem.hours, priceData);
        if (hours > 3) difference(priceData, 3, 100, priceData.hoursCount);
        if (peoples > 4) difference(priceData, 4, 50, priceData.peoplesCount);
    }

    //Подсчёт стоимости с минимальным количеством часов
    function HoursAmount(arr, priceData) {
        arr.forEach(i => {
            if (i.count == priceData.hoursCount) calc(priceData, i.price);
        })
    }
 
    //подсчёт данных с учётом доплаты
    function difference (priceData, deduction, counter,obj) {
        let difference = obj - deduction;
        priceData.totalAmount += difference * counter;
        calc(priceData, priceData.totalAmount);
    }

    //Калькулятор стоимости
    function calc(priceData, price) {
        priceData.peopleAmount = price / priceData.peoplesCount;
        priceData.totalAmount = price;
    }

    //Функция для перерендеринга результата
    function update() {
        document.querySelector('.result').innerHTML = WritePrice(price);
    }

    //Изначальный рендеринг
    CountAmount(price);

    //Событие отправки формы и вызов функции вывода результата
    const form = document.getElementById("price-count");
    form.addEventListener('submit', (e) => {
        e.preventDefault();
        CountAmount(price);
    })
}

//Массив данных прайс-листа
let arr = [
    {
        id: "1",
        days: "Mon-Thu",
        hours:
            [{count: 1, price: 120}, {count: 2, price: 200}, {count: 3, price: 250}]
    },
    {
        id: "2",
        days: "Fri-Sun",
        hours:
            [{count: 1, price: 120}, {count: 2, price: 250}, {count: 3, price: 300}]
    }
]

